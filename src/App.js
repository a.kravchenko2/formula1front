import Appbar from './components/Appbar';
import CreateEvent from './pages/CreateEvent';
import CreateRace from './pages/CreateRace';
import ShowEvents from './pages/ShowEvents';
import ShowTracks from './pages/ShowTracks';
import ShowRaces from './pages/ShowRaces';
import ShowPilots from './pages/ShowPilots';
import ShowManagers from './pages/ShowManagers';
import ShowTracksWorkload from './pages/ShowTracksWorkload';
import ShowEventTrack from './pages/ShowEventTrack';


import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import ShowRacesForTeam from './pages/ShowRacesForTeam';

function App() {
  return (
    <div className="App">
      <Router>
        <Appbar />
        <Routes>
          <Route path="/event" element={<CreateEvent />} />
          <Route path="/race" element={<CreateRace />} />
          <Route path="/allEvents" element={<ShowEvents />} />
          <Route path="/allManagers" element={<ShowManagers />} />
          <Route path="/allTracks" element={<ShowTracks />} />
          <Route path="/allRaces" element={<ShowRaces />} />
          <Route path="/pilots" element={<ShowPilots />} />
          <Route path="/races-for-team" element={<ShowRacesForTeam />} />
          <Route path="/tracks-workload" element={<ShowTracksWorkload/>} />
          <Route path="/event-track" element={<ShowEventTrack/>} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
