import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Menu from '@mui/material/Menu';

import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import Collapse from '@mui/material/Collapse';

import { useNavigate } from "react-router-dom";

export default function Appbar() {

  let navigate = useNavigate();

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [openNested, setOpenNested] = React.useState(null);
  const [anchorEl2, setAnchorEl2] = React.useState(null); 

  const clickLeftMenu = event => {
    setOpenNested(false);
    setAnchorEl2(event.currentTarget);
  };

  const redirectRoute = routePath => {
    navigate(routePath);
    setAnchorEl(null);
  };

  return (
    <Box sx={{ flexGrow: 1 }}  className = "align-start">
      <AppBar position="static" color = "error">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
            onClick={clickLeftMenu}
          >
            <MenuIcon />
          </IconButton>

{/* Left hand side */}

          <Menu
            id="menu-appbar"
            anchorEl={anchorEl2}
            open={Boolean(anchorEl2)}
            onClose={() => {setAnchorEl2(null);setOpenNested(true);}}>
            <List
                component="nav"
                aria-labelledby="nested-list-subheader"
              >
                <ListItem button onClick={() => redirectRoute("/event")}>
                  <ListItemText primary="Create Event (Admin)" />
                </ListItem>
                <ListItem button onClick={() => redirectRoute("/race")}>
                  <ListItemText primary="Create Race (Admin)" />
                </ListItem>
                <ListItem button onClick={() => redirectRoute("/allEvents")}>
                  <ListItemText primary="Show Events (Admin)" />
                </ListItem>
                <ListItem button onClick={() => redirectRoute("/allRaces")}>
                  <ListItemText primary="Show Races (Admin)" />
                </ListItem>
                <ListItem button onClick={() => redirectRoute("/allManagers")}>
                  <ListItemText primary="Show Managers (Admin)" />
                </ListItem>
                <ListItem button onClick={() => redirectRoute("/tracks-workload")}>
                  <ListItemText primary="Show Tracks Workload (Admin)" />
                </ListItem>
                <ListItem button onClick={() => redirectRoute("/allTracks")}>
                  <ListItemText primary="Show Tracks" />
                </ListItem>
                <ListItem button onClick={() => redirectRoute("/pilots")}>
                  <ListItemText primary="Show Pilots (Manager)" />
                </ListItem>
                <ListItem button onClick={() => redirectRoute("/event-track")}>
                  <ListItemText primary="Show Tracks for Events (Manager)" />
                </ListItem>
                
              </List>
          </Menu>

          <Typography  variant="h6" component="div" sx={{ flexGrow: 1 }} className = "align-end">
            Formula 1 Admin Panel
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
