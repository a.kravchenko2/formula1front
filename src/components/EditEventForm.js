import React, { useState } from "react";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import {AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import {LocalizationProvider} from '@mui/x-date-pickers/LocalizationProvider';
import Alert from '@mui/material/Alert';
import axios from 'axios';
import { FormHelperText } from "@mui/material";

const EditEventForm = ({theEvent}) => {

    const [eventId, setId] = useState(theEvent.id)
    const [statusId, setEventStatus] = useState(theEvent.status.id)
    const [eventName, setName] = useState(theEvent.eventName);
    const [startDate, setStartDate] = useState(theEvent.startDate);
    const [endDate, setEndDate] = useState(theEvent.endDate);

    const [datesIsValid, setDatesIsValid] = useState(true);
    const [nameIsValid, setNameIsValid] = useState(true);

    const [alert, setAlert] = useState(false);
  const [alertContent, setAlertContent] = useState('');

    const color = "#d00000";

    const handleStartDateChange = (newValue) => {
      setStartDate(newValue);
      if (newValue > endDate) {
        setDatesIsValid(false)
      } else {
        setDatesIsValid(true)
      }
    };
  
    const handleEndDateChange = (newValue) => {
     setEndDate(newValue);
     if (startDate > newValue) {
      setDatesIsValid(false)
    } else {
      setDatesIsValid(true)
    }
    };

    const handleInputChange = (e) => {
      setName(e.target.value)
      if (e.target.value === "") {
        setNameIsValid(false)
      } else {
        setNameIsValid(true)
      }
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        const formValues = {
          id: eventId,
          status: {
               id : statusId
           },
         eventName: eventName,
         startDate: startDate,
         endDate: endDate
       }
       console.log(formValues)
        axios.post(`http://localhost:8080/event/edit`, formValues)
          .then(res => {
            console.log(res.data);
            if (res.data === 200) {
              setAlertContent("Event updated!");
              setAlert(true);
            } else {
              setAlertContent("Oops, server error: " + res.data);
              setAlert(true);
            }
          })
          .catch((error) => {
            console.log(error);
            setAlertContent(error.toString());
            setAlert(true);
          })
      };
     return (
        <form onSubmit={handleSubmit}>
      <Grid container alignItems="center"  direction="column" spacing={2} className="mt-10 justify-center" >
        <Grid item >
          <TextField sx = {{minWidth:485}} error
            id="name-input"
            name="eventName"
            label="Event Name"
            type="text"
            value={eventName}
            onChange={handleInputChange}
          />
          <FormHelperText error  className="ml-5" id="component-helper-text">
          Name must be filled
        </FormHelperText>
        </Grid>
        <Grid item>
          <Grid container alignItems="center" justify="center" direction="row" spacing={2} >
            <Grid item>
          <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DesktopDatePicker
          name="startDate"
          label="Start Date"
          inputFormat="MM/dd/yyyy"
          type="date"
          value={startDate}
          onChange={handleStartDateChange}
          renderInput={(params) => <TextField {...params} sx={{
            svg: { color },
            input: { color },
            label: { color }}}/>}
          />
        </LocalizationProvider>
        </Grid>
        <Grid item>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DesktopDatePicker
          name="endDate"
          label="End Date"
          inputFormat="MM/dd/yyyy"
          type="date"
          value={endDate}
          onChange={handleEndDateChange}
          renderInput={(params) => <TextField {...params} sx={{
            svg: { color },
            input: { color },
            label: { color }}}/>}
          />
        </LocalizationProvider>
        </Grid>
          </Grid>
         
        </Grid>
        <Button disabled={!(nameIsValid && datesIsValid)} variant="contained" color="primary" type="submit" className="mt-20" color="error">
          Submit
        </Button>
        <Grid item >
        {alert ? <Alert className="mr-10" severity='error'>{alertContent}</Alert> : <></> }
          </Grid>
      </Grid>
    </form>

     )
}

export default EditEventForm;