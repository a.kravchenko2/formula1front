import React, { useState } from "react";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import {AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import {LocalizationProvider} from '@mui/x-date-pickers/LocalizationProvider';
import Alert from '@mui/material/Alert';
import axios from 'axios';
import { FormHelperText } from "@mui/material";



const defaultValues = {
   status: {
        id : 1
    },
  eventName: "",
  startDate: new Date(),
  endDate: new Date()
};
const Form = () => {
  const [formValues, setFormValues] = useState(defaultValues);
  const [alert, setAlert] = useState(false);
  const [alertContent, setAlertContent] = useState('');

  const [datesIsValid, setDatesIsValid] = useState(false);
  const [nameIsValid, setNameIsValid] = useState(false);

  const color = "#d00000";

  
  const handleStartDateChange = (newValue) => {
    setFormValues({
        ...formValues,
      ["startDate"]: newValue
    });
    if (newValue > formValues.endDate) {
      setDatesIsValid(false)
    } else {
      setDatesIsValid(true)
    }
  };

  const handleEndDateChange = (newValue) => {
    setFormValues({
        ...formValues,
      ["endDate"]: newValue
    });
    if (formValues.startDate > newValue) {
      setDatesIsValid(false)
    } else {
      setDatesIsValid(true)
    }
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormValues({
      ...formValues,
      [name]: value
    });
    if (value === "") {
      setNameIsValid(false)
    } else {
      setNameIsValid(true)
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log(formValues);
    axios.post(`http://localhost:8080/event/save`, formValues)
      .then(res => {
        console.log(res.data);
        if (res.data === 200) {
          setAlertContent("Event saved!");
          setAlert(true);
        } else {
          setAlertContent("Oops, server error: " + res.data);
          setAlert(true);
        }
      })
      .catch((error) => {
        console.log(error);
        setAlertContent(error.toString());
        setAlert(true);
      })
  };

  return (
    <form onSubmit={handleSubmit}>
      <Grid container alignItems="center"  direction="column" spacing={2} className="mt-10 justify-center" >
        <Grid item >
          <TextField sx = {{minWidth:485}} error
            id="name-input"
            name="eventName"
            label="Event Name"
            type="text"
            value={formValues.eventName}
            onChange={handleInputChange}
          />
          <FormHelperText error  className="ml-5" id="component-helper-text">
          Name must be filled
        </FormHelperText>
        </Grid >
        <Grid item>
          <Grid container alignItems="center" justify="center" direction="row" spacing={2} >
            <Grid item>
          <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DesktopDatePicker
          name="startDate"
          label="Start Date"
          inputFormat="MM/dd/yyyy"
          type="date"
          value={formValues.startDate}
          onChange={handleStartDateChange}
          renderInput={(params) => <TextField {...params} sx={{
            svg: { color },
            input: { color },
            label: { color }}}/>}
          />
        </LocalizationProvider>
        </Grid>
        <Grid item>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DesktopDatePicker
          name="endDate"
          label="End Date"
          inputFormat="MM/dd/yyyy"
          type="date"
          value={formValues.endDate}
          onChange={handleEndDateChange}
          renderInput={(params) => <TextField {...params} sx={{
            svg: { color },
            input: { color },
            label: { color }}}/>}
          />
        </LocalizationProvider>
        </Grid>
          </Grid>
         
        </Grid>
        <Button  disabled={!(nameIsValid && datesIsValid)} variant="contained" color="primary" type="submit" className="mt-20" color="error">
          Submit
        </Button>
        <Grid item >
        {alert ? <Alert className="mr-10" severity='error'>{alertContent}</Alert> : <></> }
          </Grid>
      </Grid>
    </form>
  );
};
export default Form;