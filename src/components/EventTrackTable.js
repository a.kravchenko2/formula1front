import React, { useState, useEffect } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Button from "@mui/material/Button";
import Paper from '@mui/material/Paper';
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";

function createData(name, type) {
 return { name, type};
}
  
const rows = [];
  
export default function EventTrackTable() {
 const [data, setData] = useState([]);

 useEffect(()=> {
    fetch("http://localhost:8080/race/event-track")
    .then(res=>res.json())
    .then((result)=>{
        setData(result);
    }
)
},[]);
  
 return (
   <>
   <TableContainer component={Paper}>
     <Table aria-label="simple table" stickyHeader>
       <TableHead>
         <TableRow>
           <TableCell>Event Name</TableCell>
           <TableCell align="left">Track Name</TableCell>
           <TableCell align="left">Track Workload</TableCell>
         </TableRow>
       </TableHead>
       <TableBody>
         {data.map((row) => (
           <TableRow key={row.id}>
             <TableCell component="th" scope="row">
               {row.eventName}
             </TableCell>
             <TableCell align="left">{row.trackName}</TableCell>
             <TableCell align="left">{row.count}</TableCell>
           </TableRow>
         ))}
       </TableBody>
     </Table>
   </TableContainer>
   </>
 );
}