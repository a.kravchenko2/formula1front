import React, { useState, useEffect } from "react";
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import CreateIcon from '@mui/icons-material/Create';
import { visuallyHidden } from '@mui/utils';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import { styled, alpha } from '@mui/material/styles';
import { red } from '@mui/material/colors';
import axios from "axios";
import EditEventForm from "./EditEventForm";
import { Modal, Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import Grid from "@mui/material/Grid";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';




const rows = [];

const headCells = [
  {
    id: 'name',
    numeric: false,
    disablePadding: false,
    label: 'Event Name',
  },
  {
    id: 'start-date',
    numeric: true,
    disablePadding: false,
    label: 'Start Date',
  },
  {
    id: 'end-date',
    numeric: true,
    disablePadding: false,
    label: 'End Date',
  },
  {
    id: 'status',
    numeric: true,
    disablePadding: false,
    label: 'Status',
  },
  {
    id: 'edit',
    numeric: true,
    disablePadding: false,
    label: '',
  },
  {
    id: 'delete',
    numeric: true,
    disablePadding: false,
    label: '',
  }
];

function EnhancedTableHead(props) {
  const { order, orderBy } =
    props;

 

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'normal'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

export default function EventsTable() {
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(3);
  const [data, setData] = useState([]);
  const [dataSize, setDataSize] = useState(0);
  const [checked, setChecked] = useState(false);
  const [sortBy, setSortBy] = useState("id")


     useEffect(()=> {
      fetch( "http://localhost:8080/event/page/" + (page > 0 ? page : 0) + "/" + sortBy)
        .then(res=>res.json())
        .then((result)=>{
            setData(result.content);
            setDataSize(result.totalElements);
        }
  )
  },[])

  const [show, setShow] = useState(false);
    
  const [selectedEvent, setSelectedEvent] = useState(0);

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const selectEvent=(event, id) => {
    var index = data.findIndex(x => x.id===id);
    setSelectedEvent(index);
    handleShow();
  }

  const removeEvent=(event, id) => {
    var array = [...data]; 
    var index = array.findIndex(x => x.id===id); //find the index of item which matches the id passed to the function
    array.splice(index, 1);

    axios.post(`http://localhost:8080/event/delete`, null, { params: { id: id } })
      .then(res => {
        console.log(res.data);
        if (res.data === 200) {
          console.log("Event removed");
          setData(array);
        } else {
          console.log("Error while removing");
        }
      })
      .catch((error) => {
        console.log(error.response);
      })

  }

  const emptyRows =
  page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;

  const switchHandler = (event) => {
    setChecked(event.target.checked);
    if (event.target.checked) {
      fetch( "http://localhost:8080/event/page/now-sorted/" + (page > 0 ? page : 0) + "/" + sortBy)
        .then(res=>res.json())
        .then((result)=>{
          setData(result.content);
            setDataSize(result.totalElements);
        }
    )
    } else {
      fetch( "http://localhost:8080/event/page/" + (page > 0 ? page : 0) + "/" + sortBy)
        .then(res=>res.json())
        .then((result)=>{
          setData(result.content);
          setDataSize(result.totalElements);        
        }
    )
    }
  };
  
const GreenSwitch = styled(Switch)(({ theme }) => ({
    '& .MuiSwitch-switchBase.Mui-checked': {
      color: red[600],
      '&:hover': {
        backgroundColor: alpha(red[600], theme.palette.action.hoverOpacity),
      },
    },
    '& .MuiSwitch-switchBase.Mui-checked + .MuiSwitch-track': {
      backgroundColor: red[600],
    },
  }));

  const handleChangePage = (event, newPage) => {
    if(checked) {
      console.log("http://localhost:8080/event/page/now-sorted/" + (newPage > 0 ? newPage : 0) + "/" + sortBy)
      fetch( "http://localhost:8080/event/page/now-sorted/" + (newPage > 0 ? newPage : 0)  + "/" + sortBy)
      .then(res=>res.json())
      .then((result)=>{
      setData(result.content);
      setDataSize(result.totalElements);
  }
)
setPage(newPage);
    } else {
      console.log("http://localhost:8080/event/page/" + (newPage > 0 ? newPage : 0) + "/" + sortBy)
      fetch( "http://localhost:8080/event/page/" + (newPage > 0 ? newPage : 0) + "/" + sortBy)
      .then(res=>res.json())
      .then((result)=>{
      setData(result.content);
      setDataSize(result.totalElements);
  }
)
setPage(newPage);
    }
    
  };

  const handleChangeRowsPerPage = (event) => {
  };

  const handleSortChange = (event) => {
    setSortBy(event.target.value)

    if (checked) {
      fetch( "http://localhost:8080/event/page/now-sorted/" + (page > 0 ? page : 0) + "/" + event.target.value)
        .then(res=>res.json())
        .then((result)=>{
          setData(result.content);
            setDataSize(result.totalElements);
        }
    )
    } else {
      fetch( "http://localhost:8080/event/page/" + (page > 0 ? page : 0) + "/" + event.target.value)
        .then(res=>res.json())
        .then((result)=>{
          setData(result.content);
          setDataSize(result.totalElements);        
        }
    )
    }
   };

  return (
    <>
    <Box sx={{ width: '100%' }}>
      <Paper sx={{ width: '100%', mb: 2 }}>
      <Grid container alignItems="center" justify="center" direction="row" spacing={2} >
          <Grid item sx = {{width: 180}}>
          <FormControl className="ml-20 mt-20" fullWidth error>
        <InputLabel id="select-event-label">Sort By</InputLabel>
        <Select
          value = {sortBy}
          label="Sort By"
          onChange={handleSortChange}
        >
       <MenuItem value="id">Id</MenuItem>
       <MenuItem value="eventName">Event Name</MenuItem>
       <MenuItem value="startDate">Start Date</MenuItem>
       <MenuItem value="endDate">End Date</MenuItem>
       <MenuItem value="status">Status</MenuItem>
        </Select>
      </FormControl>
          </Grid>
          <Grid item>
        <FormControlLabel className=" ml-20 mt-20" control={<GreenSwitch defaultChecked checked={checked} onChange={switchHandler} />} label="Only NOW Events" />
          </Grid>
      </Grid>
        <TableContainer className="mt-20">
          <Table
            sx={{ minWidth: 750 }}
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
          >
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              rowCount={rows.length}
            />
            <TableBody>
            {data.map((row) => (
           <TableRow key={row.id}>
             <TableCell component="th" scope="row">
               {row.id}
             </TableCell>
             <TableCell align="">{row.eventName}</TableCell>
             <TableCell align="right">{row.startDate}</TableCell>
             <TableCell align="right">{row.endDate}</TableCell>
             <TableCell align="right">{row.status.status}</TableCell>
             <TableCell align="center"><IconButton  onClick={(event) => removeEvent(event, row.id)} color="error" aria-label="delete">
        <DeleteIcon />
      </IconButton></TableCell>
      <TableCell align="center"><IconButton  onClick={(event) => selectEvent(event, row.id)} color="error" aria-label="delete">
        <CreateIcon />
      </IconButton></TableCell>
           </TableRow>
         ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[3]}
          component="div"
          count={dataSize}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </Box>
    <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
        </Modal.Header>
        <Modal.Body>
            <EditEventForm theEvent={data[selectedEvent]} />
        </Modal.Body>
    </Modal>
    </>
  );
}
