import React, { useState, useEffect } from "react";
import PropTypes from 'prop-types';
import { alpha } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Checkbox from '@mui/material/Checkbox';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import DeleteIcon from '@mui/icons-material/Delete';
import FilterListIcon from '@mui/icons-material/FilterList';
import { visuallyHidden } from '@mui/utils';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import { styled } from '@mui/material/styles';
import { red } from '@mui/material/colors';


function createData(managerName, teamName) {
 return { managerName, teamName};
}
  
const rows = [];
  
export default function ManagersTable() {
 const [data, setData] = useState([]);
 const [checked, setChecked] = useState(false);
  
 useEffect(()=> {
    fetch("http://localhost:8080/manager/showAll-left")
    .then(res=>res.json())
    .then((result)=>{
        setData(result);
    }
)
},[]);

const switchHandler = (event) => {
    setChecked(event.target.checked);
    if (event.target.checked) {
        fetch("http://localhost:8080/manager/showAll-inner")
        .then(res=>res.json())
        .then((result)=>{
            setData(result);
        }
    )
    } else {
        fetch("http://localhost:8080/manager/showAll-left")
        .then(res=>res.json())
        .then((result)=>{
            setData(result);
        }
    )
    }
  };
  
const GreenSwitch = styled(Switch)(({ theme }) => ({
    '& .MuiSwitch-switchBase.Mui-checked': {
      color: red[600],
      '&:hover': {
        backgroundColor: alpha(red[600], theme.palette.action.hoverOpacity),
      },
    },
    '& .MuiSwitch-switchBase.Mui-checked + .MuiSwitch-track': {
      backgroundColor: red[600],
    },
  }));

 return (
   <TableContainer component={Paper}>
            <FormControlLabel className="ml-20 mt-20" control={<GreenSwitch defaultChecked checked={checked} onChange={switchHandler} />} label="Only with team" />
     <Table aria-label="simple table" stickyHeader>
       <TableHead>
         <TableRow>
           <TableCell>Manager</TableCell>
           <TableCell align="left">Team</TableCell>
         </TableRow>
       </TableHead>
       <TableBody>
         {data.map((row) => (
           <TableRow key={row.id}>
             <TableCell component="th" scope="row">
               {row.managerName}
             </TableCell>
             <TableCell align="left">{row.teamName}</TableCell>
           </TableRow>
         ))}
       </TableBody>
     </Table>
   </TableContainer>
 );
}