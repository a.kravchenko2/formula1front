import React, { useState, useEffect } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

// function createData(name, type) {
//  return { name, type};
// }
  
// const rows = [];
  
export default function TracksTable(props) {
 const [data, setData] = useState([]);
  
 useEffect(()=> {
    fetch("http://localhost:8080/pilot/team?id=" + props.team)
    .then(res=>res.json())
    .then((result)=>{
        setData(result);
    }
)
},[]);
  


}