import React, { useState, useEffect } from "react";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Alert from '@mui/material/Alert';
import { FormHelperText } from "@mui/material";

import axios from 'axios';



const defaultValues = {
    type: "",
    date: new Date(),
    weatherConditions: "",
    event: {
        id: 1
    },
    track: {
        id : 1
    }
};

 const RaceForm = () => {
   const [formValues, setFormValues] = useState(defaultValues);
   const [events, setEvents] = useState([]);
   const [tracks, setTracks] = useState([]);
   const [alert, setAlert] = useState(false);
   const [alertContent, setAlertContent] = useState('');
   const color = "#d00000";

   const [typeIsValid, setTypeIsValid] = useState(false);
   const [datesValid, setDateIsValid] = useState(true);



    useEffect(()=> {
          fetch("http://localhost:8080/event/showAll")
          .then(res=>res.json())
          .then((result)=>{
              setEvents(result);
          }
    )
    },[])

    useEffect(()=> {
      fetch("http://localhost:8080/track/showAll" + "?name=" + "&type=")
      .then(res=>res.json())
      .then((result)=>{
          setTracks(result);
      }
      )
      },[])

   const handleDateChange = (newValue) => {
     setFormValues({
         ...formValues,
       ["date"]: newValue,
     });
    //  var index = events.findIndex(x => x.id === formValues.event.id); //find the index of item which matches the id passed to the function
    //  if (newValue > events[index].endDate || newValue < events[index].startDate) {
    //    setDateIsValid(false)
    //  } else {
    //    setDateIsValid(true)
    //  }
   };
 
   const handleEventChange = (event) => {
    setFormValues({
        ...formValues
    });
    formValues.event.id = event.target.value;
   };

   const handleTrackChange = (event) => {
    setFormValues({
        ...formValues
    });
    formValues.track.id = event.target.value;
   };

   const handleInputChange = (e) => {
     const { name, value } = e.target;
     setFormValues({
       ...formValues,
       [name]: value,
     });
     if (value === "") {
       setTypeIsValid(false)
     } else {
       setTypeIsValid(true)
     }
   };
 
   const handleSubmit = (event) => {
     event.preventDefault();
     console.log(formValues);
     axios.post(`http://localhost:8080/race/save`, formValues)
      .then(res => {
        console.log(res.data);
        if (res.data === 200) {
          setAlertContent("Race saved!");
          setAlert(true);
        } else {
          setAlertContent("Oops, server error: " + res.data);
          setAlert(true);
        }
      })
      .catch((error) => {
        console.log(error);
        setAlertContent(error.toString());
        setAlert(true);
      })
   };
 
   return (
     <form onSubmit={handleSubmit}>
       <Grid container alignItems="center"  direction="column" spacing={2} className="mt-10 justify-center" width>
         <Grid item>
           <Grid container alignItems="center" justify="center" direction="row" spacing={2} >
           <Grid item>
           <TextField error
             id="name-input"
             name="type"
             label="Race Type"
             type="text"
             value={formValues.type}
             onChange={handleInputChange}
           />
           <FormHelperText error  className="ml-5" id="component-helper-text">
          Type must be filled
        </FormHelperText>
         </Grid>
         <Grid className ="mb-20" item>
         <LocalizationProvider dateAdapter={AdapterDateFns}>
           <DesktopDatePicker
           name="date"
           label="Date"
           inputFormat="MM/dd/yyyy"
           type="date"
           value={formValues.date}
           onChange={handleDateChange}
           renderInput={(params) => <TextField  {...params} sx={{
            svg: { color },
            input: { color },
            label: { color }}} />}
         />
         </LocalizationProvider>
         </Grid>
           </Grid>
  
         </Grid>
         <Grid item sx = {{width: 400}}>
         <FormControl fullWidth error>
        <InputLabel id="select-event-label">Event</InputLabel>
        <Select
          value={formValues.event.id}
          label="Event"
          onChange={handleEventChange}
        >
          {events.map(event => {
              return (
                  <MenuItem value={event.id}>{event.eventName}</MenuItem>
              )
          })}
        </Select>
      </FormControl>
        </Grid>
        <Grid item sx = {{width: 400}}>
         <FormControl fullWidth error>
        <InputLabel id="select-track-label">Track</InputLabel>
        <Select
          value={formValues.track.id}
          label="Track"
          onChange={handleTrackChange}
        >
          {tracks.map(track => {
              return (
                  <MenuItem value={track.id}>{track.name}</MenuItem>
              )
          })}
        </Select>
      </FormControl>
        </Grid>
         <Button disabled={!(typeIsValid && datesValid)} variant="contained" color="primary" type="submit" className="mt-20" color="error">
           Submit
         </Button>
         <Grid item >
        {alert ? <Alert className="mr-10" severity='error'>{alertContent}</Alert> : <></> }
          </Grid>
       </Grid>
     </form>
   );
 };
 export default RaceForm;