import React, { useState, useEffect } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import CreateIcon from '@mui/icons-material/Create';

import EditEventForm from "./EditEventForm";
import { Modal, OverlayTrigger, Tooltip } from 'react-bootstrap';


import axios from "axios";
import EditRaceForm from "./EditRaceForm";
 
const rows = [];
  
export default function TracksTable() {
 const [data, setData] = useState([]);
  

 const [show, setShow] = useState(false);
    
 const [selectedRace, setSelectedRace] = useState(0);

 const handleShow = () => setShow(true);
 const handleClose = () => setShow(false);

 const selectRace=(event, id) => {
   var index = data.findIndex(x => x.id===id);
   setSelectedRace(index);
   console.log(data[selectedRace])
   handleShow();
 }

 useEffect(()=> {
    fetch("http://localhost:8080/race/showAll")
    .then(res=>res.json())
    .then((result)=>{
        setData(result);
    }
)
},[]);
  
const removeRace=(event, id) => {
  var array = [...data]; 
  var index = array.findIndex(x => x.id===id); //find the index of item which matches the id passed to the function
  array.splice(index, 1);

  axios.post(`http://localhost:8080/race/delete`, null, { params: { id: id } })
    .then(res => {
      console.log(res.data);
      if (res.data === 200) {
        console.log("Race removed");
        setData(array);
      } else {
        console.log("Error while removing");
      }
    })
    .catch((error) => {
      console.log(error.response);
    })

}

 return (
   <>
   <TableContainer component={Paper}>
     <Table aria-label="simple table" stickyHeader>
       <TableHead>
         <TableRow>
           <TableCell>Name</TableCell>
           <TableCell align="left">Event</TableCell>
           <TableCell align="left">Date</TableCell>
           <TableCell align="left">Type</TableCell>
           <TableCell align="left">Track</TableCell>
           <TableCell align="left">Weather</TableCell>
           <TableCell align="left">Start</TableCell>
           <TableCell align="left"></TableCell>
           <TableCell align="left"></TableCell>

         </TableRow>
       </TableHead>
       <TableBody>
         {data.map((row) => (
           <TableRow key={row.id}>
             <TableCell component="th" scope="row">
               {row.id}
             </TableCell>
             <TableCell align="left">{row.event}</TableCell>
             <TableCell align="left">{row.date}</TableCell>
             <TableCell align="left">{row.type}</TableCell>
             <TableCell align="left">{row.track}</TableCell>
             <TableCell align="left">{row.weatherConditions}</TableCell>
             <TableCell align="left">{row.canStart ? <Button variant="outlined" color="error">Start</Button> : null}</TableCell>
             <TableCell align="center"><IconButton  onClick={(event) => removeRace(event, row.id)} color="error" aria-label="delete">
        <DeleteIcon />
      </IconButton></TableCell>
      {/* <TableCell align="center"><IconButton  onClick={(event) => selectRace(event, row.id)} color="error" aria-label="delete">
        <CreateIcon />
      </IconButton></TableCell> */}
                 </TableRow>
         ))}
       </TableBody>
     </Table>
   </TableContainer>
   <Modal show={show} onHide={handleClose}>
   <Modal.Header closeButton>
   </Modal.Header>
   <Modal.Body>
       <EditRaceForm theRace={data[selectedRace]} />
   </Modal.Body>
</Modal>
</>
 );
}