import React, { useState, useEffect } from "react";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Button from "@mui/material/Button";
import Paper from '@mui/material/Paper';
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";

function createData(name, type) {
 return { name, type};
}
  
const rows = [];
  
export default function TracksTable() {
 const [data, setData] = useState([]);
 const [name, setName] = useState("");
 const [type, setType] = useState("");
  
 useEffect(()=> {
    fetch("http://localhost:8080/track/showAll" + "?name=" + name + "&type=" + type)
    .then(res=>res.json())
    .then((result)=>{
        setData(result);
    }
)
},[]);
  
const handleNameChange = (e) => {
  setName(e.target.value)
};

const handleTypeChange = (e) => {
  setType(e.target.value)
};

const handleSubmit = (event) => {
  event.preventDefault();
  fetch("http://localhost:8080/track/showAll" + "?name=" + name + "&type=" + type)
    .then(res=>res.json())
    .then((result)=>{
        setData(result);
    })
};


 return (
   <>
    <Grid className = "mt-5 ml-20" container alignItems="center" justify="center" direction="row" spacing={2} >
    <Grid item sx = {{width: 180}}>
    <TextField error
            id="name-input"
            name="name"
            label="Track Name"
            type="text"
            value={name}
            onChange={handleNameChange}
          />
    </Grid>
    <Grid item sx = {{width: 180}}>
    <TextField  error
            id="name-input"
            name="type"
            label="Type"
            type="text"
            value={type}
            onChange={handleTypeChange}
          />    </Grid>
          <Grid item className="mb-20">
          <Button variant="contained" color="primary" type="submit" className="mt-20" color="error" onClick={handleSubmit}>
          Submit
        </Button>
          </Grid>
  </Grid>
   <TableContainer component={Paper}>
     <Table aria-label="simple table" stickyHeader>
       <TableHead>
         <TableRow>
           <TableCell>Name</TableCell>
           <TableCell align="left">Type</TableCell>
         </TableRow>
       </TableHead>
       <TableBody>
         {data.map((row) => (
           <TableRow key={row.id}>
             <TableCell component="th" scope="row">
               {row.name}
             </TableCell>
             <TableCell align="left">{row.type}</TableCell>
           </TableRow>
         ))}
       </TableBody>
     </Table>
   </TableContainer>
   </>
 );
}