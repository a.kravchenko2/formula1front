import React from "react";
import EventForm from '../components/EventForm';

function CreateEvent() {
  return (
    <div>
        <EventForm/>
    </div>
  );
}

export default CreateEvent;