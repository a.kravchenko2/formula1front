import React from "react";
import RaceForm from '../components/RaceForm';

function CreateRace() {
  return (
    <div>
        <RaceForm/>
    </div>
  );
}

export default CreateRace;