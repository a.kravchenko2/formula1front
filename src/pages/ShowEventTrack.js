import React from "react";
import EventTrackTable from "../components/EventTrackTable";

function ShowEventTrack() {  
  return (
    <div>
        <EventTrackTable/>
    </div>
  );
}

export default ShowEventTrack;