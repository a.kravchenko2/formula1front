import React from "react";
import EventsTable from "../components/EventsTable";

function ShowEvents() {
  return (
    <div>
        <EventsTable/>
    </div>
  );
}

export default ShowEvents;