import React from "react";
import ManagersTable from "../components/ManagersTable";

function ShowManagers() {
  return (
    <div>
        <ManagersTable/>
    </div>
  );
}

export default ShowManagers;