import React, { useState, useEffect } from "react";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

import PilotsTable from "../components/PilotsTable";

function ShowPilots() {
    const [managers, setManagers] = useState([]);
    const [managerId, setManagerId] = useState(1);
    const [data, setData] = useState([]);
   
    useEffect(()=> {
          fetch("http://localhost:8080/manager/showAll-inner")
          .then(res=>res.json())
          .then((result)=>{
            setManagers(result);
          }
    )
    },[])

    const handleTeamChange = (event) => {
        setManagerId(event.target.value);
        fetch("http://localhost:8080/pilot/manager?id=" + event.target.value)
        .then(res=>res.json())
        .then((result)=>{
            setData(result);
        }
    )
       };
    
  
       useEffect(()=> {
          fetch("http://localhost:8080/pilot/manager?id=" + managerId)
          .then(res=>res.json())
          .then((result)=>{
              setData(result);
          }
      )
      },[]);

  return (
    <div>
         <FormControl sx={{width: 400}} error className = "mt-20 ml-20">
        <InputLabel id="select-track-label">Manager</InputLabel>
        <Select
          value={managerId}
          label="Manager"
          onChange={handleTeamChange}
        >
          {managers.map(manager => {
              return (
                  <MenuItem value={manager.id}>{manager.managerName + " (" + manager.teamName + ")"}</MenuItem>
              )
          })}
        </Select>
      </FormControl>
        {/* <PilotsTable team = {teamId}/> */}
        <TableContainer component={Paper}>
     <Table aria-label="simple table" stickyHeader>
       <TableHead>
         <TableRow>
           <TableCell>Name</TableCell>
         </TableRow>
       </TableHead>
       <TableBody>
         {data.map((row) => (
           <TableRow key={row.id}>
             <TableCell component="th" scope="row">
               {row.fullName}
             </TableCell>
           </TableRow>
         ))}
       </TableBody>
     </Table>
   </TableContainer>
    </div>
  );
}

export default ShowPilots;