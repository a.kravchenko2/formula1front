import React from "react";
import RacesTable from "../components/RacesTable";

function ShowRaces() {
  return (
    <div>
        <RacesTable/>
    </div>
  );
}

export default ShowRaces;