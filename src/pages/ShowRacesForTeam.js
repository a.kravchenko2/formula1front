import React, { useState, useEffect } from "react";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

// import RacesTable from "../components/PilotsTable";

function ShowRacesForTeam() {
  const [teams, setTeams] = useState([]);
  const [teamId, setTeamId] = useState(1);
 
  useEffect(()=> {
        fetch("http://localhost:8080/team/showAll")
        .then(res=>res.json())
        .then((result)=>{
            setTeams(result);
        }
  )
  },[])

  const handleTeamChange = (event) => {
          setTeamId(event.target.value);
     };
    


  return (
    <div>
         <FormControl sx={{width: 400}} error className = "mt-20 ml-20">
        <InputLabel id="select-track-label">Team</InputLabel>
        <Select
          value={teamId}
          label="Team"
          onChange={handleTeamChange}
        >
          {teams.map(team => {
              return (
                  <MenuItem value={team.id}>{team.name}</MenuItem>
              )
          })}
        </Select>
      </FormControl>
        {/* <RacesTable/> */}
    </div>
  );
}

export default ShowRacesForTeam;