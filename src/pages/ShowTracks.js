import React from "react";
import TracksTable from "../components/TracksTable";

function ShowTracks() {  
  return (
    <div>
        <TracksTable/>
    </div>
  );
}

export default ShowTracks;