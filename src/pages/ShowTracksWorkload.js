import React from "react";
import WorkloadTable from "../components/WorkloadTable";

function ShowTracksWorkload() {  
  return (
    <div>
        <WorkloadTable/>
    </div>
  );
}

export default ShowTracksWorkload;